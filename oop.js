class Vehicle {
   constructor ( brand, producedYear, color, engineVolume, type ){
      this.brand = brand;
      this.producedYear = producedYear;
      this.bodyType = color;
      this.engineVolume = engineVolume; 
      this.type = type;
}

  getAge() {
    return new Date().getFullYear() - this.producedYear;
  }

  getDriverLicense () {
    return (this.type === 'Car') ? 'Car driver license category B' : 'Boat driver license';
  }

}

class Car extends Vehicle { 
  constructor (brand, producedYear, color, engineVolume, bodyType){
  super(brand, producedYear, color, engineVolume, 'Car');
  this.bodyType = bodyType;
  }
} 



class Boat extends Vehicle { 
  constructor (brand, producedYear, color, engineVolume, displacement){
    super(brand, producedYear, color, engineVolume, 'Boat');
    this.displacement = displacement
  }
}




const vehicle1 = new Car('Mercedes', 2005, 'black', 2500, 'sedan');
console.log(vehicle1);

for(let key in vehicle1){
  if(vehicle1.hasOwnProperty(key))
  {console.log(key);}
}
console.log(vehicle1.getAge());

const vehicle2 = new Boat('Tracker', 2015, 'white', 1500, 2500);
console.log(vehicle2);
console.log(Object.keys(vehicle2));
console.log(vehicle2.getAge());
console.log(vehicle2.getDriverLicense());