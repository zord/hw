const Vehicle = function ( brand, producedYear, color, engineVolume, type ){
  this.brand = brand;
  this.producedYear = producedYear;
  this.bodyType = color;
  this.engineVolume = engineVolume; 
  this.type = type;
}

Vehicle.prototype.getAge = function() {
  return new Date().getFullYear() - this.producedYear;
}

Vehicle.prototype.getDriverLicense = function () {
    return (this.type === 'Car') ? 'Car driver license category B' : 'Boat driver license';
  }

const Car = function (brand, producedYear, color, engineVolume, bodyType){
  Vehicle.call(this, brand, producedYear, color, engineVolume, 'Car');
  this.bodyType = bodyType;
};

Object.setPrototypeOf(Car.prototype, Vehicle.prototype);

const Boat = function (brand, producedYear, color, engineVolume, displacement){
  Vehicle.call(this, brand, producedYear, color, engineVolume, 'Boat');
  this.displacement = displacement
};


Object.setPrototypeOf(Boat.prototype, Vehicle.prototype);

const vehicle1 = new Car('Mercedes', 2005, 'black', 2500, 'sedan');
console.log(vehicle1);

for(let key in vehicle1){
  if(vehicle1.hasOwnProperty(key))
  {console.log(key);}
}
console.log(vehicle1.getAge());

const vehicle2 = new Boat('Tracker', 2015, 'white', 1500, 2500);
console.log(vehicle2);
console.log(Object.keys(vehicle2));
console.log(vehicle2.getAge());
console.log(vehicle2.getDriverLicense());